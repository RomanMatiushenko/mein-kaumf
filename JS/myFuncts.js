function reverseNumber(num) {
    return (
        parseInt(
            num
            .toString()
            .split('')
            .reverse()
            .join('')
        ) * Math.sign(num)
    )
}


function forEach(arr, func) {
    for (let el of arr) {
        func(el);
    }
}

function map(arr, func) {
    const newArr = []
    forEach(arr, el => newArr.push(func(el)))
    return newArr;
}


function filter(arr, func) {
    const ArrFilter = []
    forEach(arr, el => func(el) ? ArrFilter.push(el) : false)
    return ArrFilter;
}

function getAdultAppleLovers(data) {
    let adultAge = 18;
    let fruit = 'apple';
    return map(filter(data, (el) => {
            return el.age > adultAge && el.favoritefruit === fruit
        }),
        (el) => el.name)
}


function getKeys(obj) {
    const Keys = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            Keys.push(key)
        }
    }
}

function getValues(obj) {
    const Arr = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            Arr.push(obj[key])
        }
    }
    return Arr
}

function showFormattedDate(dateObj) {
    const day = dateObj.getDate();
    const month = dateObj.toLocaleString('default', { month: 'short' });
    const year = dateObj.getFullYear();
    return `It is ${day}of ${month}, ${year}`
}

Array.prototype.ffunction = function(n) {
    return this.map((i) => i * n)
}

const apple = (arr) => {
    for (let i = 0; i < arr.length; i++) {
        let fruit = 'apple';
        if (arr[i].favoriteFruit === fruit) {
            let newArr = arr[i];
            return newArr;
        }
    }
};

const filt = (arr) => {
    return arr.filter(el => el.favoriteFruit === 'apple')
        .filter(el => el.age >= 10)
        .forEach(function(arrayItem) {
            let x = arrayItem.name;
            alert(x);
        });
}



const filt1 = (arr) => {
    return arr.filter(el => el.favoriteFruit === 'apple')
        .filter(el => el.age >= 10)
        .map(function(arrayItem) {
            let x = arrayItem.name;
            return x;
        });
};

const sumTo = (n) => {

    if (n === 1) {
        return n;
    } else {
        return n + sumTo(n - 1);
    }
}

const points = (arr) => {
    let result = arr.map(item => item.split(':'));
    let sumOfitems = result.map(item => item.reduce((a, b) => a - b));

    let pointsPG = sumOfitems.map(item => {
        if (item > 0) {
            item = 3;
        }
        if (item < 0) {
            item = 0;
        }
        if (item === 0) {
            item = 1;
        }
        return item;
    });

    let finalPoints = pointsPG.reduce((a, b) => a + b);
    return finalPoints;
}